import GlobalStyle from './globalStyles';
import Navbar from './components/NavBar/NavBar';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Pages from './components/Pages/Pages';

function App() {
  return (
    <Router>
      <GlobalStyle />
      <Navbar />
      <Routes>
        <Route path='/' element={<Pages />} />
        <Route path='/diensten/:path' element={<Pages />} />
        <Route path='/ovenons/:path' element={<Pages />} />
        <Route path='/informatiepredoelgroep/:path' element={<Pages />} />
        <Route path='/actueel' element={<Pages />} />
        <Route path='/agenda' element={<Pages />} />
        <Route path='/contact' element={<Pages />} />
        <Route path='/inloggen' element={<Pages />} />
      </Routes>
    </Router>
  );
}

export default App;
