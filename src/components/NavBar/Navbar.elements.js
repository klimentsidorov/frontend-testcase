import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Nav = styled.nav`
  background: #fff;
  padding: 40px 0;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1.2rem;
  position: sticky;
  top: 0;
  z-index: 99;
  width: auto;
`;

export const NavbarContainer = styled.div`
  display: flex;
  justify-content: space-between;
  height: 80px;
  max-width: 100%;
  margin-right: auto;
  margin-left: auto;
  padding-right: 50px;
  padding-left: 50px;

  @media screen and (max-width: 1200px) {
    padding-right: 30px;
    padding-left: 30px;
  }
`;

export const NavLogo = styled(Link)`
  color: black;
  justify-self: flex-start;
  cursor: pointer;
  text-decoration: none;
  display: flex;
  align-items: center;
  border-bottom: none;

  @media screen and (max-width: 1200px) {
    padding-right: 40px;
  }
`;

export const NavIcon = styled.img`
  height: 3rem;
  margin-right: 5rem;
  @media screen and (max-width: 1200px) {
    height: 2rem;
  }
`;
export const NavMag = styled.img`
  height: 2rem;
  margin: 1.5rem 0;

  @media screen and (max-width: 1200px) {
    height: 2rem;
  }
`;

export const MobileIcon = styled.div`
  display: none;
  padding: 1.1rem 0;
  color: black;
  @media screen and (max-width: 1200px) {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(-100%, 60%);
    font-size: 2rem;
    cursor: pointer;
  }
`;

export const NavMenu = styled.ul`
  display: grid;
  grid-template-columns: repeat(8, auto);
  grid-gap: 20px;
  list-style: none;
  text-align: center;
  justify-content: center;

  @media screen and (max-width: 1200px) {
    grid-template-columns: auto;
    width: 100%;
    height: auto;
    position: absolute;
    padding: 5px 0;
    top: 120px;
    left: ${({ click }) => (click ? 0 : '-100%')};
    opacity: 1;
    transition: all 0.5s ease;
    background: #fff;
  }
`;

export const NavItem = styled.li`
  height: 80px;
  border-bottom: none;
  @media screen and (max-width: 1200px) {
    width: 100%;
    &:hover {
      border: none;
    }
  }
`;

export const NavItemBtn = styled.li`
  @media screen and (max-width: 1200px) {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 120px;
  }
`;
export const NavDropdownBtn = styled.button`
  color: black;
  display: flex;
  font-size: 1.6rem;
  align-items: center;
  text-decoration: none;
  padding: 1.5rem 0.5rem;
  background: none;
  border: none;
  &:active {
    color: #e58b80;
  }

  @media screen and (max-width: 1200px) {
    text-align: center;
    padding: 2rem;
    width: 100%;
    display: table;
    font-size: 1.4rem;
    &:hover {
      color: #e58b80;
      transition: all 0.3s ease;
    }
  }
`;

export const NavDropdownContent = styled.div`
  visibility: hidden;
  transition: 0.2s;
  position: absolute;
  left: 0;
  right: 0;
  margin-top: 10px;
  background-color: #e58b80;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  z-index: 999;
`;
export const NavDropdown = styled.div`
  overflow: hidden;
  font-size: 17px;
  border: none;
  outline: none;
  color: white;
  margin: 0;
  &:hover ${NavDropdownContent} {
    visibility: visible;
    transition-delay: 0.2s;
  }
  @media screen and (max-width: 600px) {
    transition-delay: 0s;
  }
`;

export const NavLinks = styled(Link)`
  color: black;
  display: flex;
  font-size: 1.6rem;
  align-items: center;
  text-decoration: none;
  padding: 1.5rem 0.5rem;
  border-bottom: none;
  &:active {
    color: #e58b80;
  }

  @media screen and (max-width: 1200px) {
    text-align: center;
    padding: 2rem;
    width: 100%;
    display: table;
    font-size: 1.4rem;
    &:hover {
      color: #e58b80;
      transition: all 0.3s ease;
    }
  }
`;

export const NavBtnLink = styled(Link)`
  display: flex;
  justify-content: center;
  align-items: center;
  text-decoration: none;
  padding: 8px 16px;
  height: 100%;
  width: 100%;
  border: none;
  outline: none;
`;

export const Button = styled.button`
  border-radius: 40px;
  background: black;
  white-space: nowrap;
  padding: 16px 40px;
  color: #fff;
  font-size: 1.4rem;
  outline: none;
  border: none;
  font-weight: bold;
  cursor: pointer;

  &:hover {
    transition: all 0.3s ease-out;
    background: #fff;
    background-color: black;
  }

  @media screen and (max-width: 1200px) {
    width: auto;
    font-size: 1.2rem;
    padding: 8px 20px;
  }
`;
