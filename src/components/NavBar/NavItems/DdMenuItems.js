export const OvenOns = [
  {
    title: 'Never gonna give you up',
    path: 'ovenons/never-gonna-give-you-up',
  },
  {
    title: ' Never gonna run around and desert you',
    path: 'ovenons/never-gonna-run-around-and-desert-you',
  },
  {
    title: 'Never gonna say goodbye',
    path: 'ovenons/never-gonna-say-goodbye',
  },
  {
    title: 'Never gonna let you down',
    path: 'ovenons/never-gonna-let-you-down',
  },
  {
    title: 'Never gonna make you cry',
    path: 'ovenons/never-gonna-make-you-cry',
  },
  {
    title: 'Never gonna tell a lie and hurt you',
    path: 'ovenons/never-gonna-tell-a-lie-and-hurt-you',
  },
];

export const Diensten = [
  {
    title: 'Were no strangers to love',
    path: 'diensten/were-no-strangers-to-love',
  },
  {
    title: 'You know the rules and so do I',
    path: 'diensten/you-know-the-rules-and-so-do-i',
  },
  {
    title: 'A full commitments what Im thinking of',
    path: 'diensten/a-full-commitments-what-im-thinking-of',
  },
];

export const Informatiepredoelgroep = [
  {
    title: 'You wouldnt get this from any other guy',
    path: 'informatiepredoelgroep/you-wouldnt-get-this-from-any-other-guy',
  },
  {
    title: 'I just wanna tell you how Im feeling',
    path: 'informatiepredoelgroep/i-just-wanna-tell-you-how-im-feeling',
  },
  {
    title: 'Gotta make you understand',
    path: 'informatiepredoelgroep/gotta-make-you-understand',
  },
  {
    title: 'Never gonna give you up',
    path: 'informatiepredoelgroep/never-gonn-agive-you-up',
  },
  {
    title: ' Never gonna run around and desert you',
    path: 'informatiepredoelgroep/neve-rgonna-run-around-and-desert-you',
  },
  {
    title: 'Never gonna say goodbye',
    path: 'informatiepredoelgroep/never-gonna-say-goodbye',
  },
  {
    title: 'Never gonna let you down',
    path: 'informatiepredoelgroep/never-gonna-let-you-down',
  },
  {
    title: 'Never gonna make you cry',
    path: 'informatiepredoelgroep/never-gonna-make-you-cry',
  },
  {
    title: 'Never gonna tell a lie and hurt you',
    path: 'informatiepredoelgroep/never-gonna-tell-a-lie-and-hurt-you',
  },
];
