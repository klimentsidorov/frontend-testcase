import { OvenOns, Diensten, Informatiepredoelgroep } from './DdMenuItems';

export const NavDd = [
  {
    title: 'Diensten',
    items: Diensten,
  },
  {
    title: 'Informatie pre doelgroep',
    items: Informatiepredoelgroep,
  },
  {
    title: 'Oven ons',
    items: OvenOns,
  },
];

export const Navlin = [
  {
    title: 'Actueel',
    path: '/actueel',
  },
  {
    title: 'Agenda',
    path: '/agenda',
  },
  {
    title: 'Contact',
    path: '/contact',
  },
];
