import React, { useState } from 'react';
import { FaBars, FaTimes } from 'react-icons/fa';
import { IconContext } from 'react-icons/lib';
import logo from '../images/STEETS LOGO 2345.svg';
import Magnifier from '../images/001-magnifier.png';
import {
  Nav,
  NavbarContainer,
  NavLogo,
  NavIcon,
  MobileIcon,
  NavMenu,
  NavItem,
  NavLinks,
  NavMag,
  NavBtnLink,
  NavItemBtn,
  Button,
  NavDropdown,
  NavDropdownContent,
  NavDropdownBtn,
} from './Navbar.elements';
import DropdownItem from '../Dropdown/DropDownItem';
import { NavDd, Navlin } from './NavItems/NavItems';

function Navbar() {
  const [click, setClick] = useState(false);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  return (
    <>
      <IconContext.Provider value={{ color: 'black' }}>
        <Nav>
          <NavbarContainer>
            <NavLogo to='/' onClick={closeMobileMenu}>
              <NavIcon src={logo} alt='logoa' />
            </NavLogo>
            <MobileIcon onClick={handleClick}>
              {click ? <FaTimes /> : <FaBars />}
            </MobileIcon>
            <NavMenu click={click}>
              {NavDd.map((item, index) => {
                return (
                  <NavItem key={index}>
                    <NavDropdown>
                      <NavDropdownBtn>{item.title}</NavDropdownBtn>
                      <NavDropdownContent onClick={closeMobileMenu}>
                        <DropdownItem dditems={item.items} />
                      </NavDropdownContent>
                    </NavDropdown>
                  </NavItem>
                );
              })}
              {Navlin.map((item, index) => {
                return (
                  <NavItem key={index}>
                    <NavLinks to={item.path} onClick={closeMobileMenu}>
                      {item.title}
                    </NavLinks>
                  </NavItem>
                );
              })}
              <NavItem>
                <NavMag src={Magnifier} alt='Magnifier' />
              </NavItem>
              <NavItemBtn>
                <NavBtnLink to='/inloggen'>
                  <Button onClick={closeMobileMenu}>inloggen</Button>
                </NavBtnLink>
              </NavItemBtn>
            </NavMenu>
          </NavbarContainer>
        </Nav>
      </IconContext.Provider>
    </>
  );
}

export default Navbar;
