import React from 'react';
import {
  DdImg,
  DropDownItem,
  DropDownUl,
  DropdownLinks,
} from './DropDown.elements';
import Imgarrow from '../images/002-right-arrow.svg';

const DropdownItem = ({ dditems }) => {
  return (
    <>
      <DropDownUl>
        {dditems.map((item, index) => {
          return (
            <DropDownItem key={index}>
              <DdImg src={Imgarrow} alt='Arrow' />
              <DropdownLinks to={`${item.path}`}>{item.title}</DropdownLinks>
            </DropDownItem>
          );
        })}
      </DropDownUl>
    </>
  );
};

export default DropdownItem;
