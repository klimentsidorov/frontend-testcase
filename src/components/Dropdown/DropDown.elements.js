import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const DropDownUl = styled.ul`
  display: grid;
  grid-template-columns: repeat(3, auto);
  grid-gap: 1rem;
  height: auto;
  padding: 40px 0;
  list-style: none;
  justify-content: center;
  align-items: center;
  @media screen and (max-width: 1200px) {
    grid-template-columns: auto;
  }
`;

export const DropDownItem = styled.li`
  display: flex;
  justify-content: start;
  align-items: start;
  padding: 5px 100px;
`;

export const DdImg = styled.img`
  height: 0.8rem;
  margin: 3px;
`;

export const DropdownLinks = styled(Link)`
  float: none;
  color: white;
  text-decoration: none;
  font-size: 1.2rem;
  &:hover {
    color: black;
  }
`;
