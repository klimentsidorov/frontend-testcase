import React from 'react';

import { useLocation } from 'react-router-dom';
import { Container, Text } from './Pages.elements';
const Pages = () => {
  const location = useLocation();
  let path = location.pathname;

  return (
    <>
      <Container>
        {path === '/' ? (
          <Text>Wellcome to Steets</Text>
        ) : (
          <Text className='text'>You are at {path}</Text>
        )}
      </Container>
    </>
  );
};

export default Pages;
