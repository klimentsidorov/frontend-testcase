import styled from 'styled-components';

export const Container = styled.div`
  position: relative;
  top: 20rem;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Text = styled.div`
  width: 80%;
  text-align: center;
  font-size: 2rem;

  @media screen and (max-width: 1200px) {
    font-size: 1rem;
  }
`;
