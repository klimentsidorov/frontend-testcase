import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  * {
  box-sizing: border-box;
  margin: 0;
  font-family: 'Source Sans Pro', sans-serif;
 padding: 0;

 } 
 body{
 background-color: #f0f0f0;
 overflow-x: hidden;
 
 }


`;

export default GlobalStyle;
