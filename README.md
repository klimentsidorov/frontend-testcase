# Steets Frontend Development Testcase

We'd like to see how you handle this testcase. It’s a design of a header with a submenu.
Please take a look at https://klanten.steetsmooier.nl/steets/proefopdracht-front-end/index.html for a preview.

Push your code on a separate branch from master, called by your name & lastname.

A couple of notes for the development:

*    Use **SASS** and a responsive **mobile first** approach.
*    Set-up the header as a fixed bar that sticks to the top as you scroll downwards.
*    Implement the sub-menu, preferably add several sub-menus (one under “Diensten” and one under “Informatie per doelgroep” for example).
*    Use the materials provided alongside the PSD.

When you finish, please create a pull request on master.

*Important note on using git with bitbucket. Log in as a bitbucket user and clone the repository using **SSH** and then push to your branch. In order to set up **SSH** to your machine, please follow these steps: https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/

If you try and clone your repository using **HTTPS** you will not be able to push things.
